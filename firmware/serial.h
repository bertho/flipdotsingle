/*
 * Single Flipdot Driver
 *
 * Copyright (C) 2015  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __FLIPDOTSINGLE_SERIAL_H
#define __FLIPDOTSINGLE_SERIAL_H

#define have_data()	(rxbufstart != rxbufend)

#define TXBUFSIZE	(1<<6)
#define RXBUFSIZE	(1<<3)

#ifndef BAUDRATE
#define BAUDRATE	115200
#endif

#ifndef FOSC4
#error "Missing FOSC4 frequency define"
#endif

#define BRG16_VAL	((FOSC4 + BAUDRATE/2) / BAUDRATE - 1)

extern uint8_t txbuf[TXBUFSIZE];
extern uint8_t txbufstart;
extern uint8_t txbufend;
extern uint8_t rxbuf[RXBUFSIZE];
extern uint8_t rxbufstart;
extern uint8_t rxbufend;

void serial_isr(void);
void putch(uint8_t ch);
void puthex(uint8_t v);
void putx(uint8_t v);
void iputch(uint8_t ch);
uint8_t getch(void);
void puts(const char *s);

#endif
