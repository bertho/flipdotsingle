/*
 * Single Flipdot Driver
 *
 * Copyright (C) 2015  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "defs.h"
#include "serial.h"

uint8_t txbuf[TXBUFSIZE];
uint8_t txbufstart;
uint8_t txbufend;
uint8_t rxbuf[RXBUFSIZE];
uint8_t rxbufstart;
uint8_t rxbufend;

void serial_isr(void)
{
	uint8_t tmp;
	// Serial input
	if(PIR1bits.RCIF) {
		tmp = RCSTA;
		if(((rxbufend + 1) & (RXBUFSIZE-1)) != rxbufstart) {
			rxbuf[rxbufend++] = RCREG;
			rxbufend &= (RXBUFSIZE-1);
		} else {
			// Dummy read
			__asm__("banksel _RCREG");
			__asm__("movf _RCREG, w");
		}
		if(tmp & (_FERR | _OERR)) {	// Framing or overrun error
			RCSTAbits.CREN = 0;	// clear overrun
			rxbufend--;		// disregard the char
			RCSTAbits.CREN = 1;	// enable receiver
		}
	}

	// Serial output
	if(PIE1bits.TXIE && PIR1bits.TXIF) {
		TXREG = txbuf[txbufstart++];	// Send data
		txbufstart &= (TXBUFSIZE-1);
		if(txbufstart == txbufend)	// Disable int if no more data
			PIE1bits.TXIE = 0;
	}
}

void putch(uint8_t ch)
{
	cli();
	// Stall if no place in buffer
	while(((txbufend + 1) & (TXBUFSIZE-1)) == txbufstart) {
		sti();
		nop();
		nop();
		nop();
		cli();
	}
	txbuf[txbufend++] = ch;
	txbufend &= (TXBUFSIZE-1);
	PIE1bits.TXIE = 1;
	sti();
}

void puthex(uint8_t v)
{
	putch(v > 9 ? v - 10 + 'a' : v + '0');
}

void putx(uint8_t v)
{
	puthex(v >> 4);
	puthex(v & 0x0f);
}

/* Interrupt safe putch() */
void iputch(uint8_t ch)
{
	// Drop if no place in buffer
	if(((txbufend + 1) & (TXBUFSIZE-1)) == txbufstart)
		return;
	txbuf[txbufend++] = ch;
	txbufend &= (TXBUFSIZE-1);
	PIE1bits.TXIE = 1;
}

uint8_t getch(void)
{
	uint8_t x;
	cli();
	if(!have_data()) {
		sti();
		return 0;
	}
	x = rxbuf[rxbufstart++];
	rxbufstart &= (RXBUFSIZE-1);
	sti();
	return x;
}

void puts(const char *s)
{
	while(*s)
		putch(*s++);
}
