/*
 * Single Flipdot Driver
 *
 * Copyright (C) 2015  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __FLIPDOTSINGLE_DEFS_H
#define __FLIPDOTSINGLE_DEFS_H

#include <stdint.h>
#if defined(CPU_16F1509)
#include <pic16f1509.h>
#elif defined(CPU_16F1508)
#include <pic16f1508.h>
#else
#error "Missing or unsupported CPU define"
#endif

#define STRINGIZE(x)	# x
#define STRVAL(x)	STRINGIZE(x)

#ifndef FOSC
#define FOSC		(16000000UL)
#endif
#define FOSC4		(FOSC/4UL)

#define clrwdt()	__asm__("clrwdt")
#define nop()		__asm__("nop")
#define reset()		__asm__("reset")
#define cli()		do { INTCONbits.GIE = 0; } while(0)
#define sti()		do { INTCONbits.GIE = 1; } while(0)
#define ss()		(PORTCbits.RC6)
#define sck()		(PORTBbits.RB6)
#define sdi()		(PORTBbits.RB4)

#define delay_us(x)	do { uint8_t p = ((x) + 3) >> 2; while(p--) { nop(); nop(); nop(); nop(); nop(); nop(); nop(); } } while(0)

typedef union __word_t {
	struct {
		uint8_t		bl;
		uint8_t		bh;
	};
	uint16_t	w;
} word_t;

typedef union __dword_t {
	uint8_t		b[4];
	uint16_t	w[2];
	uint32_t	u;
	int32_t		i;
} dword_t;

#endif
