/*
 * Single Flipdot Driver
 *
 * Copyright (C) 2015  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "defs.h"
#include "i2c.h"
#include "config.h"

void i2c_init(void)
{
	SSP1CON1 = _CKP | _SSPM1 | _SSPM2 | _SSPM3;	// I2C slave 7-bit, clock enable, S/P ints enable
	SSP1CON2 = _SEN;				// Clock stretch enable
	SSP1CON3 = _BOEN | _AHEN | _DHEN | _PCIE | _SCIE;	// Ignore buffer overflow, hold clock on addr/data + S/P ints
	SSP1MSK  = 0b11111110;				// All 7-bit address bits signiicant
	SSP1ADD  = cfg.i2c_addr;
	SSP1STAT = 0b00000000;				// Slewrate control enable, !SMBus
	SSP1CON1bits.SSPEN = 1;

}

