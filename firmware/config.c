/*
 * Single Flipdot Driver
 *
 * Copyright (C) 2015  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "defs.h"
#include "config.h"

/* High Endurance FLASH definitions */
#if defined(CPU_16F1509)
#define HEFLASH_START	0x1f80
#define HEFLASH_SIZE	0x80
#elif defined(CPU_16F1508)
#define HEFLASH_START	0x0f80
#define HEFLASH_SIZE	0x80
#else
#error "Missing or unsupported CPU define"
#endif

config_t cfg;


uint8_t config_write(void)
{
/*
	uint8_t i;
	uint8_t sum;
	for(i = sum = 0; i < sizeof(cfg) - 1; i++) {
		sum += cfg.bytes[i];
	}
	cfg.bytes[sizeof(cfg)-1] = ~sum + 1;
*/
	__asm__("\n"
		"	movlw	LOW(_cfg)\n"
		"	movwf	_FSR0L\n"
		"	movlw	HIGH(_cfg)\n"	// Config source address
		"	movwf	_FSR0H\n"

		"	movlw	" STRVAL(CONFIG_SIZE) " - 1\n"
		"	movwf	_FSR1L\n"	// Counter
		"	clrf	_FSR1H\n"	// sum

		"__cfg_chk_loop:\n"
		"	moviw	FSR0++\n"
		"	addwf	_FSR1H, F\n"
		"	decfsz	_FSR1L, F\n"
		"	goto	__cfg_chk_loop\n"

		"	comf	_FSR1H, W\n"
		"	incf	_WREG, W\n"
		"	movwi	FSR0++\n"
	);
	cli();
	/* Erase the config area */
	__asm__("\n"
		"	banksel	_PMADRL\n"
		"	movlw	LOW(" STRVAL(HEFLASH_START) ")\n"
		"	movwf	_PMADRL\n"
		"	movlw	HIGH(" STRVAL(HEFLASH_START) ")\n"
		"	movwf	_PMADRH\n"

		"	bcf	_PMCON1, CFGS\n"
		"	bsf	_PMCON1, FREE\n"
		"	bsf	_PMCON1, WREN\n"

		"	movlw	0x55\n"
		"	movwf	_PMCON2\n"
		"	movlw	0xaa\n"
		"	movwf	_PMCON2\n"
		"	bsf	_PMCON1, WR\n"
		"	nop\n"
		"	nop\n"
		"	bcf	_PMCON1, WREN\n"
	);
	/* Write the config to the erased bank */
	__asm__("\n"
		"	movlw	LOW(_cfg)\n"
		"	movwf	_FSR0L\n"
		"	movlw	HIGH(_cfg)\n"	// Config source address
		"	movwf	_FSR0H\n"

		"	movlw	" STRVAL(CONFIG_SIZE) "\n"
		"	movwf	_FSR1L\n"	// Counter

		"	banksel	_PMADRL\n"	// Address must be lower to compensate increments
		"	movlw	LOW(" STRVAL(HEFLASH_START) ")\n"
		"	movwf	_PMADRL\n"
		"	movlw	HIGH(" STRVAL(HEFLASH_START) ")\n"
		"	movwf	_PMADRH\n"

		"	bcf	_PMCON1, CFGS\n"
		"	bsf	_PMCON1, WREN\n"
		"	bsf	_PMCON1, LWLO\n"
		"	goto	__cfg_write_loop_entry\n"

		"__cfg_write_loop:\n"
		"	incf	_PMADRL, F\n"
		"__cfg_write_loop_entry:\n"
		"	moviw	FSR0++\n"
		"	movwf	_PMDATL\n"
		"	movlw	0x34\n"		// RETLW opcode high byte
		"	movwf	_PMDATH\n"

		"	movlw	0x55\n"		// Write latches
		"	movwf	_PMCON2\n"
		"	movlw	0xaa\n"
		"	movwf	_PMCON2\n"
		"	bsf	_PMCON1, WR\n"
		"	nop\n"
		"	nop\n"

		"	decfsz	_FSR1L, F\n"
		"	goto	__cfg_write_loop\n"

		"	bcf	_PMCON1, LWLO\n"	// Select flash write
		"	movlw	0x55\n"
		"	movwf	_PMCON2\n"
		"	movlw	0xaa\n"
		"	movwf	_PMCON2\n"
		"	bsf	_PMCON1, WR\n"
		"	nop\n"
		"	nop\n"
		"	bcf	_PMCON1, WREN\n"
	);
	sti();
	return PMCON1;
}

uint8_t cfgcnt;

uint8_t config_read(void)
{
	uint8_t i;
	uint8_t sum;
	__asm__("\n"
		"	movlw	LOW(_cfg)\n"
		"	movwf	_FSR0L\n"
		"	movlw	HIGH(_cfg)\n"	// Store address
		"	movwf	_FSR0H\n"

		"	movlw	LOW(" STRVAL(HEFLASH_START) " + 0x8000)\n"
		"	movwf	_FSR1L\n"
		"	movlw	HIGH(" STRVAL(HEFLASH_START) " + 0x8000)\n"
		"	movwf	_FSR1H\n"

		"	banksel	_cfgcnt\n"
		"	movlw	" STRVAL(CONFIG_SIZE) "\n"
		"	movwf	_cfgcnt\n"	// Counter

		"__cfg_read_loop:\n"
		"	moviw	FSR1++\n"
		"	movwi	FSR0++\n"
		"	decfsz	_cfgcnt, F\n"
		"	goto	__cfg_read_loop\n"
	);
#if 0
	__asm__("\n"
		"	movlw	LOW(_cfg)\n"
		"	movwf	_FSR0L\n"
		"	movlw	HIGH(_cfg)\n"	// Store address
		"	movwf	_FSR0H\n"

		"	movlw	" STRVAL(CONFIG_SIZE) "\n"
		"	movwf	_FSR1L\n"	// Counter

		"	banksel	_PMADRL\n"
		"	movlw	LOW(" STRVAL(HEFLASH_START) ")\n"
		"	movwf	_PMADRL\n"
		"	movlw	HIGH(" STRVAL(HEFLASH_START) ")\n"
		"	movwf	_PMADRH\n"

		"__cfg_read_loop:\n"
		"	bcf	_PMCON1, CFGS\n"
		"	bcf	_PMCON1, RD\n"
		"	nop\n"
		"	nop\n"
		"	movf	_PMDATL, W\n"
		"	movwi	FSR0++\n"
		"	movf	_PMDATH, W\n"	// Dummy read

		"	incf	_PMADRL, F\n"	// Next address
		"	decfsz	_FSR1L, F\n"
		"	goto	__cfg_read_loop\n"
	);
#endif

	for(sum = i = 0; i < sizeof(cfg); i++) {
		sum += cfg.bytes[i];
	}

	if(cfg.signature[0] != CONFIG_SIG0 || cfg.signature[1] != CONFIG_SIG1 || sum) {
		cfg.signature[0] = CONFIG_SIG0;
		cfg.signature[1] = CONFIG_SIG1;
		cfg.i2c_addr = DEF_I2C_ADDR;
	}
	return sum;
}

