/*
 * Single Flipdot Driver
 *
 * Copyright (C) 2015  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "mem.h"

void clear_ram(void)
{
	__asm__("\n"
		"	movlw	0x20\n"		// FSR0 = 0x2000, start of linear RAM
		"	movwf	FSR0H\n"
		"	clrf	FSR0L\n"
		"	clrf	FSR1L\n"	// FSR1L is counter
		"__clear_loop:\n"
		"	clrf	INDF0\n"
		"	addfsr	FSR0, 1\n"
		"	decfsz	FSR1L, f\n"
		"	goto	__clear_loop\n"
		);
#ifdef CPU_16F1509
	/* The 16F1509 has 512 bytes RAM, continue loop once more */
	__asm__(
		"__clear_loop2:\n"
		"	clrf	INDF0\n"
		"	addfsr	FSR0, 1\n"
		"	decfsz	FSR1L, f\n"
		"	goto	__clear_loop2\n"
		);
#endif
}

