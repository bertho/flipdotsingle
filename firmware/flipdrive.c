/*
 * Single Flipdot Driver
 *
 * Copyright (C) 2015  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "defs.h"
#include "mem.h"
#include "serial.h"
#include "i2c.h"
#include "config.h"

static __code uint16_t __at _CONFIG1 config1 = _FOSC_INTOSC & _WDTE_SWDTEN & _PWRTE_OFF & _MCLRE_ON & _CP_OFF & _BOREN_SBODEN & _CLKOUTEN_OFF & _IESO_OFF & _FCMEN_OFF;
static __code uint16_t __at _CONFIG2 config2 = _WRT_OFF & _STVREN_OFF & _BORV_HI & _LPBOR_OFF & _DEBUG_OFF & _LVP_ON;

#define supply(x)	do { PORTCbits.RC0 = (x); } while(0)
#define fdset(x)	do { PORTCbits.RC1 = (x); } while(0)
#define fdres(x)	do { PORTCbits.RC2 = (x); } while(0)
#define pwr(x)		do { PORTAbits.RA5 = (x); } while(0)

/*
 * Flip-capacitor -> ADC sees voltage + 1.19V with boost converter over
 * 22k/(22k+91k) @5V:
 * 16.00V -->  685 counts
 * 18.00V -->  765 counts
 * 20.00V -->  845 counts
 * 22.00V -->  925 counts
 * 24.00V --> 1004 counts
 * 24.47V --> 1023 counts
 */
#define ADCTHRESHOLD_16	685
#define ADCTHRESHOLD_18	765
#define ADCTHRESHOLD_20	845
#define ADCTHRESHOLD_22	925
#define ADCTHRESHOLD_24	1004

#define ADCTHRESHOLD	ADCTHRESHOLD_20		// FIXME: make dynamic

#define F_CMDDATA	0x01
#define F_CMDFLIP	0x02
#define F_FLIPSET	0x04
#define F_ADCVAL	0x08
#define F_TIMER		0x10

static uint8_t modespi;		// Input mode: SPI(1) or I2C(0)
static uint8_t modeser;		// Input mode: Serial(1) or BitIO(0)
static uint8_t modevcc;		// Power supply mode Vcc(1) or Vin(0)

static uint8_t flags;

enum {
	FS_IDLE = 0,
	FS_ENABLEBOOST,
	FS_CHARGING,
	FS_FLIP,
	FS_TIMING
};

static uint8_t flipstate;	// State if the flip-state-machine
static uint8_t flipsmdir;	// State-machine flip direction data

typedef union __cmddata_t {
	struct {
		uint8_t count : 6;	// Bits 0..5
		uint8_t flipdir : 1;	// Bit 6
		uint8_t flipena : 1;	// Bit 7
	};
	uint8_t data;
} cmddata_t;

static cmddata_t sspcmd;

static uint8_t isr_tmp;
static word_t adcval;

static inline void spi_isr(void)
{
	isr_tmp = SSP1BUF;		// Get the incoming byte
	sspcmd.count--;			// Count one down for downstream
	SSP1BUF = sspcmd.data;		// Pass the previous data along
	sspcmd.data = isr_tmp;
	PIR1bits.SSP1IF = 0;		// Reset interrupt flag
}

enum {
	IST_IDLE,
	IST_DATARD_ACK,
	IST_DATARD,
	IST_DATAWR_ACK,
	IST_DATAWR,
};

static uint8_t i2cstate;

static inline void i2c_isr(void)
{
	PIR1bits.SSP1IF = 0;	// Clear interrupt
	if(SSP1STATbits.BF) {
		isr_tmp = SSP1BUF;	// Get the data
	} else if(i2cstate == IST_IDLE && SSP1STATbits.S) {
		// FIXME: What about Restart conditions?
		return;
	}
	if(SSP1STATbits.P) {
		i2cstate = IST_IDLE;	// End of transmission, reset state
		return;
	}

	switch(i2cstate) {
	case IST_IDLE:
		if(SSP1STATbits.D_NOT_A) {
			// Data byte is unexpected, nack and ignore
			SSP1CON2bits.ACKDT = 1;	// Set NACK
			break;
		}
		if(SSP1STATbits.R_NOT_W) {	// Read byte from register and check R/W
			i2cstate = IST_DATARD_ACK;
		} else
			i2cstate = IST_DATAWR_ACK;
		SSP1CON2bits.ACKDT = 0;	// Set ACK
		break;
	case IST_DATARD_ACK:
		i2cstate = IST_DATARD;
		SSP1BUF = sspcmd.data;	// Read, send back last command
		break;
	case IST_DATAWR_ACK:
		i2cstate = IST_DATAWR;
		break;
	case IST_DATARD:
		if(!SSP1CON2bits.ACKSTAT) {
			SSP1BUF = sspcmd.data;	// Keep sending back last command
		} else
			i2cstate = IST_IDLE;			// Data nack'ed, end of transmission
		break;
	case IST_DATAWR:
		sspcmd.data = isr_tmp;	// Store the data
		if(!sspcmd.count)
			flags |= F_CMDDATA;	// We have a command ready
		i2cstate = IST_IDLE;
		SSP1CON2bits.ACKDT = 0;	// Set ACK
		break;
	}
	SSP1CON1bits.CKP = 1;		// Release clock
}

void isr(void) __interrupt
{
	// SPI/I2C input
	if(PIR1bits.SSP1IF) {
		if(modespi)
			spi_isr();
		else
			i2c_isr();
	}

	// External interrupt (SS/FLIP went high)
	if(INTCONbits.INTF) {
		INTCONbits.INTF = 0;
		if(modeser) {
			if(modespi) {
				if(!sspcmd.count)
					flags |= F_CMDDATA;	// Addressed to us, inform main
			}
		} else {
			if(sdi())
				flags |= F_FLIPSET | F_CMDFLIP;
			else
				flags |= F_CMDFLIP;
		}
	}

	// ADC on timer0 interval
	if(INTCONbits.TMR0IF) {
		INTCONbits.TMR0IF = 0;
		adcval.bl = ADRESL;
		adcval.bh = ADRESH;
		ADCON0bits.GO = 1;	// Next conversion
		flags |= F_ADCVAL;
	}

	/* Flip timer */
	if(PIE1bits.TMR1IE && PIR1bits.TMR1IF) {
		PIE1bits.TMR1IE = 0;
		PIR1bits.TMR1IF = 0;
		T1CONbits.TMR1ON = 0;	// Disable the timer
		flags |= F_TIMER;

		fdset(0);	// Insurance that it is off...
		fdres(0);
	}

	serial_isr();
}

static uint8_t toupper(uint8_t ch)
{
	return ch >= 'a' && ch <= 'z' ? ch - ('a' - 'A') : ch;
}

static uint8_t hexval(uint8_t ch)
{
	return ch > '9' ? ch - 'A' + 10 : ch - '0';
}

static void handle_serial(void)
{
	uint8_t ch = toupper(getch());
	putch(ch);
	/* FIXME: do something... */
}

static void do_flip(uint8_t dir)
{
	if(flipstate != FS_IDLE)
		return;
	flipsmdir = dir;
	flipstate = modevcc ? FS_ENABLEBOOST : FS_FLIP;
}

static void settimer(void)
{
#define FLIPTIMER	4000	// 2ms
	// FIXME: make it dependent on voltage
	TMR1L = (uint8_t)((-FLIPTIMER) & 0xff);
	TMR1H = (uint8_t)(((-FLIPTIMER) >> 8) & 0xff);

	PIR1bits.TMR1IF = 0;
	PIE1bits.TMR1IE = 1;
	T1CONbits.TMR1ON = 1;
}

static void statemachine(void)
{
	switch(flipstate) {
	case FS_IDLE:
		break;

	case FS_ENABLEBOOST:
		supply(0);
		pwr(1);
		flipstate = FS_CHARGING;
		break;

	case FS_CHARGING:
		if(adcval.w >= ADCTHRESHOLD) {
			flipstate = FS_FLIP;
		}
		break;

	case FS_FLIP:
		pwr(0);
		supply(0);		// Turn off Vin pass-through
		settimer();
		if(flipsmdir)
			fdset(1);	// Flip to "on"
		else
			fdres(1);	// Flip to "off"
		flipstate = FS_TIMING;
		break;

	case FS_TIMING:
		if(flags & F_TIMER) {
			flags &= ~F_TIMER;
			fdset(0);
			fdres(0);
			if(!modevcc)
				supply(1);
			flipstate = FS_IDLE;
		}
		break;
	}
}

void main(void)
{
	uint8_t x;

	cli();

	clear_ram();

	PORTA = 0;
	PORTB = 0b10000000;	// High on TX line == idle
	PORTC = 0;

	OPTION_REG = _INTEDG | _PS2 | _PS1 | _PS0;	// Rising edge ext int, enable WPU, prescaler 1:256 to timer0 on Fosc/4
	INTCONbits.INTE = 1;	// Enable external interrupt (connected to SS)

	OSCCON = _SCS1 | _IRCF3 | _IRCF2 | _IRCF1 | _IRCF0;	// 16MHz internal clock
	WDTCON = _SWDTEN | _WDTPS2 | _WDTPS1 | _WDTPS0;		// WDT on at 128ms timeout
	clrwdt();

	TRISA = 0b11011100;	// Outputs: PWR(RA5), PGC(RA1), PGD(RA0)
	TRISB = 0b01111111;	// Outputs: TXD(RB7)
	TRISC = 0b01111000;	// Outputs: SDO(RC7), FDRES(RC2), FDSET(RC1), SUPPLY(RC0)
	ANSELA = 0b00010000;	// AN3 (RA4) analog
	ANSELB = 0;		// Port B digital
	ANSELC = 0;		// Port C digital
	WPUA = 0b00000100;	// Pull-up on SS(RA2/RC6)
	WPUB = 0b00100000;	// Pull-up on RXD
	// port c has no pull-ups

	x = config_read();

	// Setup serial port
	TXSTA = _BRGH | _TXEN;
	BAUDCON = _BRG16;
	SPBRGH = (uint8_t)(BRG16_VAL >> 8);
	SPBRGL = (uint8_t)(BRG16_VAL & 0xff);
	RCSTA = _CREN | _SPEN;

	// Read the operational mode from the jumpers
	modespi = PORTCbits.RC5;	// Non-zero if mode is I2C and zero for SPI
	modeser = PORTCbits.RC4;	// Non-zero if mode is serial and zero for bit-io
	modevcc = PORTCbits.RC3;	// Non-zero if mode is Vcc and zero for Vin

	if(modeser) {
		if(modespi) {
			SSP1CON1 = _SSPM2;	// SPI slave with SS mode 0
			SSP1CON3 = _BOEN;	// SPI overwrite data
			SSP1STAT = _CKE;	// Sample edge hi-lo
			SSP1BUF = 0x00;		// Default to "no-data"
			while(sck()) {		// Wait for SCK line to idle (Datasheet section 21.2.4)
				nop();
			}
			SSP1CON1bits.SSPEN = 1;
		} else {
			i2c_init();
		}
	}


	// Timer 0 handles timing of ADC's flipdot supply voltage measurement
	INTCONbits.TMR0IE = 1;		// TMR0 interrupt enable
	TMR0 = 0;
	INTCONbits.TMR0IF = 0;

	// ADC measurement of voltage
	ADCON1 = _ADFM | 0b00100000;	// Right adjust, Fosc/32
	ADCON2 = 0;			// No autotrigger
	ADCON0 = _ADON | _CHS1 | _CHS0;	// ADC on channel 3
	ADCON0bits.ADON = 1;		// Start a conversion before timer 0 fires

	// Flip timing on timer 1
	T1CON = _T1CKPS0;		// prescale 1:2 on Fosc/4

	PIR1 = 0;			// Clear any pending interrupts
	PIE1 = _RCIE;			// Enable serial port receive ints
	if(modeser)
		PIE1bits.SSP1IE = 1;	// and MSSP if serial mode

	i2cstate = IST_IDLE;

	INTCONbits.PEIE = 1;		// Enable peripheral interrupts
	sti();

	delay_us(250);		// Delay to ensure idle serial line for about one character

	puts("\r\n\r\nSingle FLipdot Driver - at your service\r\n"
	     "v20150506, (c) 2015 Vagrearg, under GPLv3\r\n");
	if(modespi) {
		puts("mode SPI\r\n> ");
	} else {
		puts("mode I2C, 8-bit address (W, R): 0x");
		putx(cfg.i2c_addr);
		puts(", 0x");
		putx(cfg.i2c_addr+1);
		puts("\r\n> ");
	}

	if(x) {
		puts("config sum wrong (");
		putx(x);
		puts(") set to default\r\n> ");
	}

	while(1) {
		clrwdt();

		statemachine();

		if(flags & F_CMDDATA) {
			if(sspcmd.flipena)
				do_flip(sspcmd.flipdir);
			flags &= ~F_CMDDATA;
		}

		if(flags & F_CMDFLIP) {
			do_flip(flags & F_FLIPSET);
			flags &= ~(F_CMDFLIP | F_FLIPSET);
		}

		if(flags & F_ADCVAL) {
			flags &= ~F_ADCVAL;
		}

		if(have_data()) {
			handle_serial();
		}

	}
}
