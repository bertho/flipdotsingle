/*
 * Single Flipdot Driver
 *
 * Copyright (C) 2015  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __FLIPDOTSINGLE_CONFIG_H
#define __FLIPDOTSINGLE_CONFIG_H

#define CONFIG_SIZE	32
#define CONFIG_SIG0	'C'
#define CONFIG_SIG1	'f'

#ifndef DEF_I2C_ADDR
#define DEF_I2C_ADDR	(0x44 << 1)
#endif

typedef union __config_t {
	struct {
		uint8_t		signature[2];
		uint8_t		i2c_addr;
		uint8_t		reserved[CONFIG_SIZE - 4];
		uint8_t		checksum;
	};
	uint8_t	bytes[CONFIG_SIZE];
} config_t;

extern config_t cfg;

uint8_t config_read(void);
uint8_t config_write(void);

#endif
